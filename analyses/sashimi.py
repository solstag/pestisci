from sashimi import GraphModels as _GraphModels

from ..data import load_df as _load_df

from sashimi.blocks.better_network_map import network_map


def load():
    df = (
        _load_df()
        .dropna(subset="tokens")
        .explode(["paragraph", "tokens"])
        .reset_index()
    )
    corpus = _GraphModels(
        config_path="config.json",
        token_sources=["tokens"],
        col_time="Start date",
        col_title="paragraph",
        load_data=False,
    )
    corpus.load_data(df, name="pestisci")
    corpus.process_sources()
    corpus.load_domain_topic_model()
    corpus.register_config()
    return corpus


def network(corpus):
    network_map(
        corpus,
        block_labels=[
            *{
                k
                for k, v in corpus.label_to_tlblock.items()
                if v[0] == "doc" and v[1] == 1
            }
        ],
        characteristic={"ter": [1]},
        output_file=corpus.blocks_adir / "bnet_test.pdf",
    )


def chained(corpus, chain_col="Title"):
    corpus.set_chain(chain_col)
    corpus.load_domain_chained_model()
    corpus.register_config()
    corpus.domain_map(chained=True)
