import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from ..data import load_df as _load_df


def joint_histogram_tokens():
    plt.close("all")

    df = _load_df().dropna(subset="tokens").explode(["paragraph", "tokens"])

    chosen = set(df["tokens"].explode().value_counts().head(20).index)

    tokens = df["tokens"].map(lambda tokens: [t for t in tokens if t in chosen])
    tokens = tokens.to_frame().assign(cross_tokens=tokens)
    tokens = tokens.explode("tokens").explode("cross_tokens")
    tokens = tokens.loc[tokens["tokens"] != tokens["cross_tokens"]]
    tokens = tokens.reset_index()

    fig = sns.jointplot(
        data=tokens,
        x="tokens",
        y="cross_tokens",
        kind="hist",
        joint_kws={"discrete": (True, True)},
        marginal_kws={"discrete": (True, True)},
    )
    fig.ax_joint.tick_params(rotation=45)
    return fig


def joint_histogram_tokens_countries():
    plt.close("all")
    country_col = "Organization_country"

    df = _load_df().dropna(subset="tokens").explode(["paragraph", "tokens"])

    chosen = set(df["tokens"].explode().value_counts().head(20).index)

    df["tokens"] = df["tokens"].map(lambda tokens: [t for t in tokens if t in chosen])
    df = df.explode("tokens").explode(country_col)
    df = df.reset_index()

    fig = sns.jointplot(
        data=df,
        x="tokens",
        y=country_col,
        kind="hist",
        stat="",
        joint_kws={"discrete": (True, True)},
        marginal_kws={"discrete": (True, True)},
    )
    fig.ax_joint.tick_params(rotation=45)
    return fig


def cross_heatmap():
    plt.close("all")
    country_col = "Organization_country"
    df = _load_df().dropna(subset="tokens").explode(["paragraph", "tokens"])
    chosen = set(df["tokens"].explode().value_counts().head(20).index)
    df["tokens"] = df["tokens"].map(lambda tokens: [t for t in tokens if t in chosen])
    df = df.explode("tokens").explode(country_col)
    df = df.reset_index()

    ct = pd.crosstab(
        index=df["tokens"], columns=df[country_col], normalize="columns"
    )  # 'index'
    ax = sns.heatmap(ct)
    ax.tick_params(rotation=45)
    return ax
