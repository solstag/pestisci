from itertools import tee
import logging
from pathlib import Path
import re
import readline
import zipfile

import pandas as pd
import pdfminer.high_level as pdfminer_hl
import spacy

logger = logging.getLogger(__name__)

# Restrict delims so completion works with paths.
readline.set_completer_delims("\t\n")
readline.parse_and_bind("tab: complete")

DATA_DIR = None


def load_df():
    data_dir = get_base_data_dir()
    df = pd.read_parquet(data_dir / "pestisci.parquet.zst")
    return df


def store_df(df):
    data_dir = get_base_data_dir()
    df.to_parquet(data_dir / "pestisci.parquet.zst", compression="zstd")


def build_df(docs=None):
    # Load the metadata CSV
    zip_path = get_source_data_zip_path() / "NGO_reports.csv"
    with zip_path.open() as f:
        df = pd.read_csv(f, sep="\t")
    df["Start date (complete date)"] = pd.to_datetime(
        df["Start date (complete date)"], format="%d/%m/%Y"
    )
    df["Start date"] = df["Start date"].astype("Int16")
    for col in (
        k for k in df if k.startswith("Organization") or k.startswith("Person")
    ):
        df[col] = df[col].str.split(r"\*\*\*")

    # Now add the text
    if docs is True:
        docs = process_documents()
    if docs is not None:
        docs = {d["path"].stem.split("-")[0].strip(): d for d in docs}
        for field in ["paragraphs", "tokens"]:
            df[field] = (
                df["Folder name"]
                .map(lambda name: docs[name].get(field, None))
                .map(list, na_action="ignore")
            )
    return df


def process_documents(tokens=True):
    rxs = get_regexes()
    vocab = get_proto_vocabulary()
    try:
        nlp = spacy.load("en_core_web_trf")
    except Exception:
        nlp = spacy.load("en_core_web_sm")

    for path, text in read_texts():
        doc = {"path": path}
        text = clean_text(rxs, path, text)
        paragraphs = process_paragraphs(rxs, path, text)
        unbroken_paragraphs = unbreak_words(rxs, vocab, paragraphs)
        if tokens:
            unbroken_paragraphs_tee = tee(unbroken_paragraphs)
            tokenized_paragraphs = tokenize_paragraphs(nlp, unbroken_paragraphs_tee[0])
            relevant_paragraphs_tokens = (
                (par, toks)
                for par, toks in zip(unbroken_paragraphs_tee[1], tokenized_paragraphs)
                if len(toks) > 4
            )
            relevant_paragraphs, relevant_tokens = [
                *zip(*relevant_paragraphs_tokens)
            ] or ([], [])
            doc["paragraphs"], doc["tokens"] = relevant_paragraphs, relevant_tokens
        else:
            doc["paragraphs"] = unbroken_paragraphs
        yield doc


def process_paragraphs(rxs, path, text):
    # For most documents, single line breaks continue a paragraph and double+ line
    # breaks separate them. But some broken documents have double linebreaks almost
    # everywhere instead of only between paragraphs, requiring us to differentiate
    # double from triple+ line breaks in order to find and fix them.
    split3 = [
        [rxs["single_nl"].split(text2) for text2 in rxs["double_nl"].split(text3)]
        for text3 in rxs["triple+_nl"].split(text)
    ]

    # Broken documents have double linebreaks almost everywhere instead of only
    # between paragraphs. We use heuristics to find and fix such documents.
    num_valid_lines = sum(
        sum(bool(rxs["multi_word"].search(line)) for line in s1)
        for s2 in split3
        for s1 in s2
    )
    num_valid_s1 = sum(
        any(rxs["multi_word"].search(line) for line in s1) for s2 in split3 for s1 in s2
    )
    ratio = num_valid_s1 and (num_valid_lines / num_valid_s1)
    broken = ratio < 3 / 2
    if broken:
        print("\n--", broken, num_valid_lines, num_valid_s1, ratio)
        logger.warning(f"Broken file: {path}")
        max_line_length = max(len(line) for s2 in split3 for s1 in s2 for line in s1)
        len_previous_line = None

    paragraphs = []
    if not broken:
        for s2 in split3:
            for s1 in s2:
                paragraphs.append(" ".join(line.strip() for line in s1))
    else:
        for s2 in split3:
            for s1 in s2:
                has_single_linebreaks = len(s1) > 1
                if has_single_linebreaks:
                    paragraphs.append(" ".join(line.strip() for line in s1))
                    len_previous_line = None
                else:
                    line = s1[0].strip()
                    if len_previous_line is None:
                        paragraphs.append(line)
                    elif len_previous_line > 0.5 * max_line_length and not rxs[
                        "paragraph_end"
                    ].search(paragraphs[-1][-1]):
                        paragraphs[-1] += " " + line
                    else:
                        paragraphs.append(line)
                    len_previous_line = len(line)

    return paragraphs


def get_base_data_dir():
    global DATA_DIR
    if DATA_DIR is None:
        print(f"Current directory: {Path().absolute()}")
        DATA_DIR = Path(input("Enter data dir (RET for current directory): "))
    return DATA_DIR


def get_source_data_zip_path():
    return zipfile.Path(
        get_base_data_dir() / "Pesti-Sci_NGO_reports.zip", "Pesti-Sci_NGO_reports/"
    )


def get_proto_vocabulary():
    vocab = set()
    for path, text in read_texts():
        vocab.update(
            w.casefold() for s in text.split("\n") for w in re.split(r"\W", s)[1:-1]
        )
        return vocab


def get_regexes() -> dict[str, re.Pattern]:
    flags = re.VERBOSE | re.IGNORECASE | re.DOTALL
    res = {}
    res["triple+_nl"] = r"\n (?:\s*\n){2,}"
    res["double_nl"] = r" \n    \s*\n"
    res["single_nl"] = r" \n"
    res["paragraph_end"] = r"[.!?] \s* $"
    res["word_break"] = r"(\w+) (?<=[^_\d]) -[ ]+ (?=[^_\d]) (\w+)"
    res["multi_word"] = r"[^\s]+ (?: \s+ [^\s]+){3}"
    res[
        "references"
    ] = r"""
               (?:^|\n) [\W\d]* (?P<ref>references|notes) \s* :? \s* (?:\n|$)
        (?! .*      \n  [\W\d]* (?P=ref)                  \s* :? \s* (?:\n|$) )"""
    # res["url"] = r"[\[\(<]?http[^\s\]\)>]+"
    # res["cite"] = r"^ \s* \d+ \s*"
    rxs = {k: re.compile(v, flags=flags) for k, v in res.items()}
    return rxs


def read_texts(extract_to_files=True):
    """
    Return the texts of PDF files inside a zipfile.

    Start by looking for the results of a previous extraction, and if none is found
    perform an extraction with pdfminer.

    If an extraction is performed and `extract_to_files` is `True`, stores the results so
    they can be reused.
    """
    source_zip_paths = (
        p
        for p in get_source_data_zip_path().iterdir()
        if p.filename.suffix.casefold() == ".pdf"
    )
    out_dir = get_base_data_dir() / "extracted_text"
    out_dir.mkdir(exist_ok=True)
    for source_zip_path in source_zip_paths:
        out_path = out_dir / source_zip_path.filename.with_suffix(".txt").name
        if out_path.exists():
            text = out_path.read_text()
        else:
            with source_zip_path.open(mode="rb") as f:
                text = pdfminer_hl.extract_text(f)
            if extract_to_files:
                out_path.write_text(text)
        yield source_zip_path.filename, text


def clean_text(rxs, path, text, audit=False):
    text = re.sub(r"‑", "-", text)
    text = re.sub(r" ", " ", text)
    text = re.sub(r"", "\n\n\n", text)  # page break?
    match_references = rxs["references"].search(text)
    if match_references:
        if audit:
            print(
                f"\n==\nDropping references:\n--\n{text[match_references.start():]}\n"
            )
            input()
        return text[: match_references.start()]
    else:
        return text


def unbreak_words(rxs, vocab, paragraphs):
    paragraphs = iter(paragraphs)
    par = next(paragraphs)

    def keep_hyphen(word_before, word_after):
        return f"{word_before}{word_after}".casefold() not in vocab and (
            (word_after.casefold() in vocab and len(word_after) > 2)
            or word_after[0].isupper()
        )

    def word_unbreak_sub(m):
        unbroken = f"{m[1]}-{m[2]}" if keep_hyphen(m[1], m[2]) else f"{m[1]}{m[2]}"
        return unbroken

    for next_par in paragraphs:
        if (
            len(par) > 1
            and par[-1] == "-"
            and par[-2].isalpha()
            and next_par[0].isalpha()
            and next_par[0].islower()
        ):
            par = par[:-1]
            par_last_word = re.split(r"\W", par)[-1]
            next_par_first_word = re.split(r"\W", next_par)[0]
            if keep_hyphen(par_last_word, next_par_first_word):
                par += "-"
            par = par + next_par
        else:
            yield rxs["word_break"].sub(word_unbreak_sub, par)
            par = next_par
    yield rxs["word_break"].sub(word_unbreak_sub, par)


def tokenize_paragraphs(nlp, paragraphs):
    def choose_tokens(tokens):
        for token in tokens:
            if token.pos_ in ("ADJ", "ADV", "VERB"):
                yield token

    for paragraph in paragraphs:
        tokens = nlp(paragraph)
        chosen_tokens = choose_tokens(tokens)
        yield [token.lemma_ for token in chosen_tokens]
